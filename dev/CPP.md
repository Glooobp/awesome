## **C++**

C++ is a general-purpose language created by Bjarne Soustrup in 1983.

### **Learn**
- 📘 [LearnCpp.com awesome course (🇬🇧)](https://www.learncpp.com/)
- 📘 [Zeste de Savoir cours C++ (🇫🇷)](https://zestedesavoir.com/tutoriels/822/la-programmation-en-c-moderne/)

### **Misc**
- 📘 [The C++ Reference (🇬🇧)](https://en.cppreference.com/w/)
- 🔧 [The Cmake building tool](https://cmake.org/)
- 🔧 [The QTcreator IDE](https://www.qt.io/download-open-source)
- 💼 [The Ncurses lib : term utils](https://invisible-island.net/ncurses/)

### **Graphic apps**
- 💼 [QT popular graphics framework](https://www.qt.io/)
