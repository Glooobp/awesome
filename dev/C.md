## **C**

C is a very old and low level compiled language, invented by Richard Stallman in the 70's.

### **Learn**
- 📘 [C from scratch (🇫🇷)](https://www.youtube.com/playlist?list=PLo1MmTvqMbiUcyrms0_XPtVKYsvT-4y81)
