## **Dev**

Some awesome things for developpers.

### **Code sharing and group working**
- 📘 [Principe de review de code (🇫🇷)](https://www.jesuisundev.com/code-review/)

### **Git**
- 📘 [A simple Git guide (🇬🇧)](https://rogerdudler.github.io/git-guide/)
- 📘 [Un petit cours sur le fonctionnement de Git (🇫🇷)](https://www.youtube.com/playlist?list=PLvWq8NyUnHac-wKi6Ft8wiod9dHMMMfC2)

### **Language building**
- 📘 [Fonctionnement et implémentation d'un langage (🇫🇷)](https://totodu.net/Compilation/Compilation)
