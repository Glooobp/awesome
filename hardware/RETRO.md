## **Retrocomputing**

Retrocomputing is the best hobby of the world (really) !

### **Misc**
- 📘 [Un site web très intéressant (🇫🇷)](https://video.latavernedejohnjohn.fr/)
- 📘 [Une chaîne Peertube sur le sujet (🇫🇷)](https://www.jonathandupre.fr/)

### **CPUs**
- 📘 [CPU World processor database (🇬🇧)](https://www.cpu-world.com/index.html)
