## **Advanced Linux usages**

Somethings for advanced linux users

### **Misc**
- 📘 [Build software from sources (🇬🇧)](http://moi.vonos.net/linux/beginners-installing-from-source/)
- 🔧 [Neofetch : view some system infos](https://github.com/dylanaraps/neofetch)
- 📘 [Quelle est la différence entre terminal, console et shell ? (🇫🇷)](https://qastack.fr/ubuntu/506510/what-is-the-difference-between-terminal-console-shell-and-command-line)

### **The Kernel**
- 📘 [The Kernel Documentation (🇬🇧)](https://www.kernel.org/doc/html/latest/)

### **Build Distros**
- 📂 [Linux From Scratch : build your own Linux distro (🇬🇧)](https://linuxfromscratch.org/)
- 📂 [Linux From Scratch : faites votre propre distro Linux (🇫🇷)](http://fr.linuxfromscratch.org/)
