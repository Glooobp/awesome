## **Linux Beginners**

You are a GNU/Linux beginner ? Here's your place !

### **Basics**
- 📘 [Reprenez le controle avec Linux (Openclassrooms) (🇫🇷)](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux)
- 📘 [Qu'est ce que Linux ? (🇫🇷)](https://www.youtube.com/watch?v=xLhCOFWaY_M)
- 📂 [Essayer Linux Ubuntu ou l'installer (🇫🇷)](https://www.youtube.com/watch?v=kZS84ctzii8)

### **Terminal**
- 📘 [Commandes Linux de base (🇫🇷)](https://doc.ubuntu-fr.org/tutoriel/console_commandes_de_base)
