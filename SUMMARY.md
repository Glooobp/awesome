# **SUMMARY**

```
📘 Docs
🔧 Tools
💼 Frameworks / Libs              
📂 Tutorials
💬 Forums and Networks
```

### **Dev**
- [Misc](dev/MISC.md) 📘
- [C++](dev/CPP.md) 📘💼🔧
- [C](dev/C.md) 📘
- [JS](dev/JS.md) 💼
- [Rust](dev/RUST.md) 📘

### **GNU/Linux**
- [Beginners](linux/BEGINNERS.md) 📘📂
- [Bash/Shell](linux/BASH.md) 📘
- [Advanced Linux usages](linux/SYSTEM.md) 🔧📂

### **Low Level**
- [Assembly](low_level/ASM.md) 📘🔧📂💬
- [OS dev](low_level/OSDEV.md) 📘💬

### **Cyber security**
- [Misc](cybersec/MISC.md) 📘
- [Authentification](cybersec/AUTH.md) 🔧

### **Hardware**
- [PC Configuration and optimization](hardware/CONFIG.md) 🔧
- [Architectures and low level](hardware/ARCH.md) 💬
- [Retrocomputing](hardware/RETRO.md) 📘

### **Repairing**
- [Repair PCs](repairing/REPAIR.md) 📘📂
- [Restore](repairing/RESTORE.md) 📘📂

### **Other**
- [Machine Learning and Deep learning](other/IA.md) 📘

### **Off-topic**
- [Music](off_topic/MUSIC.md) 🔧
