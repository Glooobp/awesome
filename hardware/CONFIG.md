## **PC Configuration and optimization**

PC configuration, overclocking, optimization and more...

### **Misc**
- 🔧 [An awesome tool to design PC configs and pick parts (🇬🇧)](https://pcpartpicker.com/)
- 📘 [Hardware news (🇬🇧)](https://videocardz.com/)

### **Building**
- 📘 [Montage PC et électricité statique (🇫🇷)](https://www.jonathandupre.fr/articles/35-videos-youtube-peertube/117-bob-morris-et-l-esd)
