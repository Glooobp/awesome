## **Assembly**

Assembly, or as we sometimes call it, the machine language, is the lowest langage, witch can discuss directly with the processor. Learn ASM can help you to understand how your system really works.

### **Learn**
- 📘 [Concepts de low level et rudiments d'ASM 80x86 (🇫🇷)](https://benoit-m.developpez.com/assembleur/tutoriel/#LI)

### **The NASM Assembly language**
- 📘 [Official doc (🇬🇧)](https://www.nasm.us/xdoc/2.15.05/nasmdoc.pdf)
- 🔧 [Official website](https://www.nasm.us/)
- 📂 [A simple NASM tutorial (🇬🇧)](https://cs.lmu.edu/~ray/notes/nasmtutorial/)

### **Misc**
- 🔧 [An Atom package witch provides ASM syntax support](https://atom.io/packages/language-x86-64-assembly)

### **About**
- 💬 [Low Level / Assembly Discord Server (🇬🇧)](https://discord.gg/XcKXGfgky9)
