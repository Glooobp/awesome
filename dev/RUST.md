## **Rust**

Rust is a multi-purposes and multiparadigm programming language build by Mozilla.

### **Learn**
- 📘 [The Rust official book (🇬🇧)](https://doc.rust-lang.org/stable/book/)
- 📘 [Rust In Action : learn Rust with system programming (🇬🇧)](https://www.rustinaction.com/)

### **Misc**
- 📘 [The Rust reference (🇬🇧)](https://doc.rust-lang.org/stable/reference/)
