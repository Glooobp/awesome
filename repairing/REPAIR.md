## **Repair PCs**

You can go to the repair shop, but repairing your PC yourself is more economic and interesting !

### **Methods**
- 📂 [Petit tableau regroupant les premières procédures de dépannage (🇫🇷)](http://www.mediafire.com/file/3xvjlcg7brvrmki/Tuto_D%25C3%25A9pannage_Informatique.pdf/file)
- 📘 [Utilisation d'une POST-card pour le dépannage (🇫🇷)](https://latavernedejohnjohn.fr/articles/40-astuces/134-toi-aussi-rejoins-la-force-de-la-post-card)
