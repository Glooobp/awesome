## **OS Dev**

Dev an operating system is the Graal of every developers...

### **Misc**
- 📘 [The OSDev wiki (🇬🇧)](https://wiki.osdev.org/Expanded_Main_Page)
- 📘 [Le wiki DevSE (en cours de rédaction) (🇫🇷)](https://devse.wiki/)

### **About**
- 💬 [DevSE Discord Server (🇫🇷)](http://discord.devse.wiki/)
